<?php

class utils
{

	/**
	* Allows autoloading of an unloaded module by adding its name to the resource paths
	*
	* To load multiple modules, simply add more arguments to the function call
	*
	* @param	string	$module_name
	*/
	public static function load_module($module)
	{
		// grab the existing modules
			$modules = Kohana::config('core.modules');
			
		// add the new module(s)
			$new_modules = func_get_args();
			foreach($new_modules as $module)
			{
				array_push($modules, MODPATH . $module);
			}
			
		// update the core settings
			Kohana::config_set('core.modules', array_unique($modules));
	}
	
	public static function format_code($file, $language = 'php', $echo = TRUE)
	{
		switch(strtolower($language))
		{
			case 'php':
				// preg_replace callback function
					function replace_comments($matches)
					{
						return '<span style="color:green">' .strip_tags($matches[0]). '</span>';
					}
					
				// get content
					$content	= htmlentities(file_get_contents($file));
					
				// replace content
					$content	= preg_replace('%(([\'"]).+?\\2)%', '<b>$1</b>', $content); // strings
					$content	= preg_replace('%(\$\w[\w\d]*)%', '<span style="color:black;font-weight:bold">$1</span>', $content); // variables
					$content	= preg_replace('%(\b[A-Z_]{2,}\b)%', '<span style="color:blue">$1</span>', $content); // constants
					$content	= preg_replace_callback('%(//.+)([\r\n]+)%', 'replace_comments', $content); // comments
					$content	= preg_replace_callback('%(/\*\*.+?\*/)%s', 'replace_comments', $content); // comments
			break;
				
			case 'css':
			/*
				// preg_replace callback function
					function replace_comments($matches)
					{
						return '<span style="color:green">' .strip_tags($matches[0]). '</span>';
					}
					
				// get content
					$content	= htmlentities(file_get_contents($file));
					
				// replace content
					//$content	= preg_replace('%[{;]\s*([\w-]+):%', '<b>$1</b>', $content); // properties
					$content	= preg_replace('%[{;/]\s*([\w-]+):([^;}]+)%', '<span style="color:black;font-weight:bold">$1</span>', $content); // variables
					$content	= preg_replace('%(\b[A-Z_]{2,}\b)%', '<span style="color:blue">$1</span>', $content); // constants
					$content	= preg_replace_callback('%(//.+)([\r\n]+)%', 'replace_comments', $content); // comments
				*/
			break;
				
			case '':
			break;
				
			default:
		}
		
		// echo
			if($echo)
			{
				echo $content;
			}
	
		// return
			return $content;
	}

	/**
	* Title
	* 
	* Description
	*
	* @param	
	* @param	
	* @param	
	* @param	
	*/
}
?>