<?php

class arr extends arr_Core
{
	/**
	* Creates a deep and unreferenced copy of an array
	*
	* @param	
	* @param	
	*/
	public static function deep_copy (&$arr, &$copy) 
	{
		// checks
			// return if NULL
				if($arr == NULL)return false;

			// create empty array if copy is null
				if(!is_array($copy))$copy = array();
	
		// copy keys
			foreach($arr as $k => $v) 
			{
				if(is_array($v)) 
				{
					array_deep_copy($v, $copy[$k]);
				}
				else 
				{
					$copy[$k] = $v;
				}
			}
	}
	
	
	/**
	* Filters an existing array by key name to return an array with only those keys 
	* that match the criteria (regular expression or stringcomparison)
	*
	* @param	
	* @param	
	*/
	public static function filter_by_key($arr_in, $obj)
	{
		$arr_out	= array();
		
		// if a regular expression is handed in
			if(is_string($obj) && substr($obj, 0, 1) == '/' && substr($obj, -1, 1) == '/')
			{
				foreach($arr_in as $k => $v)
				{
					if(preg_match($obj, $k))
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// if is an array, check the key names against the array
			else if(is_array($obj))
			{
				foreach($arr_in as $k => $v)
				{
					if(in_array($k, $obj))
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// if not do a straight string comparison
			else
			{
				foreach($arr_in as $k => $v)
				{
					if(strstr($k, $obj) !== FALSE)
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// return		
			return $arr_out;
	}
	
	
	/**
	* Reorder an array according to a subsequent key-order you specify
 	* Key order can be a numeric or associative array, or most simply a comma-delimeted string
	*
	* @param	arr_in		The input array
	* @param	$keys		The 
	* @param	
	* @param	
	*/
	public static function reorder_by_key($arr_in, $keys, $matches_only = false)
	{
	
		// variables
		
			// key keys from a string
				if(is_string($keys))
				{
					if(function_exists($keys))
					{
						$fn				= $keys;
						$keys			= NULL;
						$matches_only	= TRUE;
					}
					else
					{
						$keys = preg_split('%,\s*%', $keys);
					}
				}
				else
				{
					// should be be an array
					// print_r(array_values(array_keys($arr)));
				}
					
			// output array
				$arr_out	= array();
		
		// process
				
			// if keys are provided, extract correct keys from input array
				if($keys)
				{
					foreach($keys as $k)
					{
						if($arr_in[$k])
						{
							$arr_out[$k] = $arr_in[$k];
							unset($arr_in[$k]);
						}
					}
				}
					
			// if a function is provided, get the key indexes via the function
				elseif($fn)
				{
					// index cache
						$index_cache = array();
						
					// loop through the input array's keys derive each one's index via the sort function
						foreach($arr_in as $key => $value)
						{
							$index	= $fn($key);
							$index_cache[$index] = $key;
						}
							
					// sort the indices, then loop through and build final array
						ksort($index_cache);
						foreach($index_cache as $index => $key)
						{
							$arr_out[$key] = $arr_in[$key];
						}
				}
					
		// return
					
			// return reordered array, plus remaining keys
				return $matches_only ? $arr_out : array_merge($arr_out, $arr_in);
					
	}
	
	
	/**
	* Transposes an array.
	*
	* Imagine taking a 10x3 table and swapping rows and columns to return a 3x10 table
	*
	* @param		a single array, or a list of arrays as multiple arguments
	*/
	public static function transpose()
	{
	
		// variables
			$input_arrays	= func_get_args();
			$output_rows	= array();
			
		// if a single (associative) array is passed, grab it into the top-level array
			$input_rows		= count($input_arrays) == 1 ? $input_arrays[0] : $input_arrays;

		// loop left-to-right through the columns (by colIndex)
			$col_index = 0;
			do
			{
				// new array to grab current column values
					$cur_col = array();
					
				// loop top-to-bottom thorugh rows of current column (by rowName)
					foreach($input_rows as $row_name => $row_values)
					{
						$col_value			= $row_values[$col_index];
						$cur_col[$row_name]	= $col_value;
					}

				// add curCol to outputRows if values existed in current column
					if($col_value !== NULL)
					{
						array_push($output_rows, $cur_col);
						$col_index++;
					}
			}
			while($col_value !== NULL);

		// return
			return $output_rows;
	}
	
	
	/**
	* Convert a numeric array of words to an associative array, optionally converting
	* both the keys and values to different cases. Useful for things like converting a 
	* bunch of options to "key name" and "Value name"
	*
	* @param	arr_in					a single numeric array
	* @param	values_to_sentence		create values in Sentence case
	* @param	keys_to_lower			create keys in lower case
	*/
	public static function to_associative($arr_in, $values_to_sentence = TRUE, $keys_to_lower = TRUE)
	{
		$arr_out = array();
		foreach($arr_in as $val)
		{
			$arr_out[$keys_to_lower ? strtolower($val) : $val] = str_replace('_', ' ', $values_to_sentence ? ucwords($val) : $val);
		}
		return $arr_out;
	}


	
	/**
	* Exports an array to nicely-formatted, parsable, PHP code
	*
	* echo arr::export(array(1,2,3, 'test' => array(1,2,3)), 'config');
	*/
	public static function export($arr, $name = 'config', $php_tags = TRUE)
	{
		// anonymous functions
			$uncuddle_brackets	= create_function('$matches', '$indent = substr($matches[1], 1); return "array\n$indent(\n$indent	";');
			$cuddle_array		= create_function('$matches', 'return "=> array";');
			
		// prettify output
			$str = var_export($arr, TRUE);
			$str = str_replace('  ', '	', $str);
			$str = preg_replace('/\d+ => [\r\n]?\t?/', '', $str);
			$str = preg_replace_callback('/array \([\r\n]+(\t+)/', $uncuddle_brackets, $str);
			$str = preg_replace_callback('/=>\s+array/', $cuddle_array, $str);
			
		// add name
			$str = '$' . $name . ' = ' . $str .';';
			
		// add tags
			if($php_tags)
			{
				$str = "<?php\n" . $str . "\n?>";
			}
		
		// return
			return $str;
	}
	
	
	/**
	* Title
	* 
	* Description
	*
	* @param	
	* @param	
	* @param	
	* @param	
	*/

}
/*echo "<pre>";
$config = array('config' => array('save' => TRUE, 'url' => 'http://www.google.com'), 'files' => array('image.jpg', 'document.txt', 'application.exe'), array(1,2,3));
echo arr::export($config, 'config');
*/
?>