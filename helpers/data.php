<?php //defined('SYSPATH') or die('No direct script access.');

class data_Core
{

	/**
	* CSV to Array
	*
	* Converts CSV files to PHP arrays
	* 
	* @param	file			The path to the file
	* @param	header_row		Specifies if the CSV file has a header row or not. If it does, an associative array is returned
	* @param	include_blanks	Includes blank rows as empty arrays
	*/
	public static function csv_to_array($file, $header_row = TRUE, $include_blanks = FALSE, $delimiter = ','){
	
		// variables
			$row			= 0;
			$arr_headings	= array();
			$arr_rows		= array();
			$arr_row		= array();
		
		// parse file
			if($fp = fopen ($file,"r"))
			{
				while ($data = fgetcsv($fp, 10000, $delimiter))
				{
				
					// clean leading and trainling spaces
						$data	= array_map('trim', $data);
						
					// get headings
						if($row == 0)
						{
							$arr_headings = $data;
							//continue;	// skip rest of loop // fails - why?
						}
					
					// parse entries	
						$arr_row = array();
						for ($i = 0; $i < count($data); $i++) 
						{
							$heading	= @$arr_headings[$i];
							if($heading != NULL && ($data[$i] != NULL || $include_blanks))
							{
								if(isset($arr_row[$heading]))
								{
									if(is_string($arr_row[$heading]))
									{
										$arr_row[$heading] = array($arr_row[$heading]);
									}
									array_push($arr_row[$heading], $data[$i]);
								}
								else
								{
									$arr_row[$heading] = $data[$i];
								}
							}
						}
				
					// print
						if($row > 0)
						{
							if(count(array_keys($arr_row)) > 0)
							{
								array_push($arr_rows, $arr_row);
							}
						}
							
					// update
						$row++;
						
				}
					
			// cleanup
				fclose ($fp); 
				
			// return
				return $arr_rows;
			}
			else
			{
				return NULL;
			}
	}
	
	
	
	/**
	* Array to CSV
	*
	* Converts arrays to CSV format
	* 
	* @param	arr				The array to convert
	* @param	header_row		Add a header ro based on the array's keys to the file
	*/
	public static function array_to_csv($arr, $header_row = TRUE)
	{
	
		// variables
			$csv				= '';
			$arr_headings		= array();


		// get header row
			if($header_row)
			{
				foreach($arr[0] as $heading => $cell)
				{
					if(!is_numeric($heading))
					{
						array_push($arr_headings, '"' .$heading. '"');
					}
				}
				$csv .= implode(',', $arr_headings)."\r\n";
			}

		// rows
			foreach($arr as $row)
			{
			
				// variables
					$arr_cells = array();
			
				// columns
					foreach($row as $heading => $cell)
					{
						if(is_null($cell))
						{
						}
						else if(!is_numeric($cell))
						{
							$cell	= str_replace("\'", "'", $cell);
							$cell	= str_replace('\"', '"', $cell);
							$cell	= str_replace('"', '""', $cell);
							$cell	= '"' .$cell. '"';
						}
						array_push($arr_cells, $cell);
					}
						
				// assign values to csv
					$csv .= implode(',', $arr_cells) . "\r\n";
			}
				
		// return
			$csv .= "\r\n";
			return $csv;
	}
}

?>